import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data


class Autoencoder:

    def __init__(self, n_epochs=30001, batch_size=500, display_step=500):

        self.n_inputs = 784
        self.n_neurons_hidden1 = 256
        self.n_neurons_hidden2 = 128
        self.learning_rate = 0.002
        self.batch_size = batch_size
        self.mnist = input_data.read_data_sets("/tmp/data", one_hot=True)

        self.weights = {
          'encoder1': tf.Variable(tf.truncated_normal([self.n_inputs, self.n_neurons_hidden1], stddev=0.1)),
          'encoder2': tf.Variable(tf.truncated_normal([self.n_neurons_hidden1, self.n_neurons_hidden2], stddev=0.1)),
          'decoder1': tf.Variable(tf.truncated_normal([self.n_neurons_hidden2, self.n_neurons_hidden1], stddev=0.1)),
          'decoder2': tf.Variable(tf.truncated_normal([self.n_neurons_hidden1, self.n_inputs], stddev=0.1)),
        }
        self.bias = {
          'encoder1': tf.Variable(tf.zeros([self.n_neurons_hidden1])),
          'encoder2': tf.Variable(tf.zeros([self.n_neurons_hidden2])),
          'decoder1': tf.Variable(tf.zeros([self.n_neurons_hidden1])),
          'decoder2': tf.Variable(tf.zeros([self.n_inputs])),
        }

        # Criação de placeholder para entrada
        self.X = tf.placeholder('float', [None, self.n_inputs])  # Nome da variavel 'X' é considerado padrão na comun

        self.result_encoder = self.run_encoder(self.X)
        self.result_decoder = self.run_decoder(self.result_encoder)

        y_pred = self.result_decoder
        y_true = self.X
        self.loss = tf.reduce_mean(tf.pow(y_true - y_pred, 2))
        self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
        self.saver = tf.train.Saver()

        # Roda o treinamento
        with tf.Session() as sess:
            # Inicializador de variaveis
            sess.run(tf.global_variables_initializer())
            # Realiza o treinamento
            for i in range(0, n_epochs):

                batch_x, _ = self.mnist.train.next_batch(batch_size=batch_size)

                # Roda o optimizer e o loss, setando o placeholder 'X' com o proximo batch
                _, iter_loss = sess.run([self.optimizer, self.loss], feed_dict={self.X: batch_x})

                if i % display_step == 0:
                    self.run_and_print(i)
                    print("\nXXXXXX AUTOENCODER XXXXXX")
                    print("Epoch :", i)
                    print("Discriminator loss :", iter_loss)
                    self.sess_save = self.saver.save(sess=sess, save_path="./saved_state/model.ckpt")

    def run_encoder(self, x):
        layer1 = tf.nn.relu(tf.add(tf.matmul(x, self.weights['encoder1']), self.bias['encoder1']))
        layer2 = tf.nn.sigmoid(tf.add(tf.matmul(layer1, self.weights['encoder2']), self.bias['encoder2']))

        return layer2

    def run_decoder(self, x):
        layer1 = tf.nn.relu(tf.add(tf.matmul(x, self.weights['decoder1']), self.bias['decoder1']))
        layer2 = tf.nn.sigmoid(tf.add(tf.matmul(layer1, self.weights['decoder2']), self.bias['decoder2']))

        return layer2

    def get_result_encoder(self):
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            self.saver.restore(sess=sess, save_path="./saved_state/model.ckpt")
            batch_x, _ = self.mnist.train.next_batch(batch_size=self.batch_size)
            result = sess.run(self.result_encoder, feed_dict={self.X: batch_x})

            return(result)

    def get_result_decoder(self, x):
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            self.saver.restore(sess=sess, save_path="./saved_state/model.ckpt")
            for element in x:
                result = sess.run(self.result_decoder, feed_dict={self.result_encoder: element})

            self.print_img(result)
            # self.run_and_print("final")
            return result

    def print_img(self, predicted):
        n_images = 1
        image_canvas_prev = np.empty((28 * n_images, 28 * n_images))

        for i in range(n_images):

            for j in range(n_images):
                image_canvas_prev[(i * 28):((i + 1) * 28), (j * 28):((j + 1) * 28)] = predicted[j].reshape([28, 28])

        plt.figure(figsize=(n_images, n_images))
        plt.imsave("./final_result.png", image_canvas_prev, origin="upper", cmap="gray")

    def run_and_print(self, n):
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            self.saver.restore(sess=sess, save_path="./saved_state/model.ckpt")

            n_images = 4
            image_canvas_prev = np.empty((28 * n_images, 28 * n_images))

            for i in range(n_images):
                batch_x, _ = self.mnist.train.next_batch(batch_size=self.batch_size)

                predicted = sess.run(self.result_decoder, feed_dict={self.X: batch_x})

                for j in range(n_images):
                    image_canvas_prev[(i * 28):((i + 1) * 28), (j * 28):((j + 1) * 28)] = predicted[j].reshape([28, 28])

            plt.figure(figsize=(n_images, n_images))
            plt.imsave("images_result_autoencoder_train/result_{}.png".format(n), image_canvas_prev, origin="upper",
                       cmap="gray")

#
# # RUN
# ae = Autoencoder(n_epochs=3001, batch_size=1000, display_step=500)
#
# ae.run_once(batch_size=100)
