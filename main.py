import autoencoder
import gan
import time

n_epochs_autoencoder = 5001
n_epochs_gan = 50001

init_time = time.time()

my_autoencoder = autoencoder.Autoencoder(n_epochs=n_epochs_autoencoder, batch_size=500, display_step=50)
autoencoder_trainig_time = time.time() - init_time

my_gan = gan.GAN(n_epochs=n_epochs_gan, batch_size=50, display_step=500, x=my_autoencoder.get_result_encoder())
gan_training_time = time.time() - autoencoder_trainig_time

result_gan = my_gan.get_generator_img()
img = my_autoencoder.get_result_decoder(result_gan)

print("\n\nXXXXX Execution time XXXXX")
print("traing autoencoder time : ", autoencoder_trainig_time, " epochs :", n_epochs_autoencoder)
print("traing gan time : ", gan_training_time, " epochs: ", n_epochs_gan)