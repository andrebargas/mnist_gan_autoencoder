import tensorflow as tf
from tensorflow import nn
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data


class GAN:

    def __init__(self, n_epochs=3001, batch_size=1000, display_step=500, x=None):

        # Macro variáveis
        self.x = x
        self.batch_size = batch_size
        self.mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)
        self.learning_rate = 0.001
        self.n_neurons_hidden = 393
        self.z_dimension = 100
        self.n_output_discriminator = 1
        self.n_inputs = 128

        # Entrada para Descriminador
        self.X = tf.placeholder(tf.float32, [None, self.n_inputs], name='X')

        # Entrada para o Gerador
        self.Z = tf.placeholder(tf.float32, [None, self.z_dimension], name='Z')

        self.weights = {
            'discriminator1': tf.Variable(tf.truncated_normal([self.n_inputs, self.n_neurons_hidden], stddev=0.1),
                                          name='d_weight1'),
            'discriminator2': tf.Variable(tf.truncated_normal([self.n_neurons_hidden, self.n_output_discriminator], stddev=0.1),
                                          name='d_weight2'),
            'generator1': tf.Variable(tf.truncated_normal([self.z_dimension, self.n_neurons_hidden], stddev=0.1),
                                      name='g_weight1'),
            'generator2': tf.Variable(tf.truncated_normal([self.n_neurons_hidden, self.n_inputs], stddev=0.1),
                                      name='g_weight2')
        }

        self.bias = {
            'discriminator1': tf.Variable(tf.zeros([self.n_neurons_hidden]), name= 'd_bias1'),
            'discriminator2': tf.Variable(tf.zeros([self.n_output_discriminator]), name= 'd_bias2'),
            'generator1': tf.Variable(tf.zeros([self.n_neurons_hidden]), name= 'g_bias1'),
            'generator2': tf.Variable(tf.zeros([self.n_inputs]), name= 'g_bias2')
        }

        var_list_generator = [self.weights['generator1'], self.weights['generator2'], self.bias['generator1'],
                              self.bias['generator2']]
        var_list_discriminator = [self.weights['discriminator1'], self.weights['discriminator2'],
                                  self.bias['discriminator1'], self.bias['discriminator2']]

        self.generator_result = self.run_generator(self.Z)
        self.discriminator_result_true, self.discriminator_logits_true = self.run_discriminator(self.X)
        self.discriminator_result_fake, self.discriminator_logits_fake = self.run_discriminator(self.generator_result)

        # LOSS
        self.loss_discriminator_true = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.discriminator_logits_true,
                                                    labels=tf.ones_like(self.discriminator_logits_true)))
        self.loss_discriminator_fake = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.discriminator_logits_fake,
                                                    labels=tf.zeros_like(self.discriminator_logits_fake)))
        self.loss_discriminator = self.loss_discriminator_true + self.loss_discriminator_fake

        self.loss_generator = tf.reduce_mean(nn.sigmoid_cross_entropy_with_logits(logits=self.discriminator_logits_fake,
                                                                                  labels=tf.ones_like(
                                                                                    self.discriminator_logits_fake)))
        # OTIMIZER
        self.optimizer_discriminator = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss_discriminator,
                                                                                                            var_list=var_list_discriminator)

        self.optimizer_generator = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss_generator,
                                                                                                        var_list=var_list_generator)
        self.saver = tf.train.Saver()


        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            for i in range(n_epochs):

                _, discriminator_loss_curr = sess.run([self.optimizer_discriminator, self.loss_discriminator],
                                                      feed_dict={self.X: x, self.Z: self.sample_z(batch_size, self.z_dimension)})
                _, generator_loss_curr = sess.run([self.optimizer_generator, self.loss_generator],
                                                  feed_dict={self.X: x, self.Z: self.sample_z(batch_size, self.z_dimension)})

                if i % display_step == 0:
                    print("\nXXXXXX GAN XXXXXX")
                    print("Epoch :", i)
                    print("Discriminator loss :", discriminator_loss_curr)
                    print("Generator loss :", generator_loss_curr)
                    self.save_path = self.saver.save(sess=sess, save_path="./saved_state/model_gan.ckpt")

    def sample_z(self, m, n):
        return np.random.uniform(-1., 1., size=[m, n])

    def run_generator(self, z):
        layer1 = tf.nn.relu(tf.matmul(z, self.weights['generator1']) + self.bias['generator1'])
        logit = tf.matmul(layer1, self.weights['generator2']) + self.bias['generator2']
        layer2 = tf.nn.sigmoid(logit)

        return layer2

    def run_discriminator(self, x):
        layer1 = tf.nn.relu(tf.matmul(x, self.weights['discriminator1']) + self.bias['discriminator1'])
        logit = tf.matmul(layer1, self.weights['discriminator2']) + self.bias['discriminator2']
        layer2 = tf.nn.sigmoid(logit)
        return layer2, logit

    def get_generator_img(self):
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            self.saver.restore(sess=sess, save_path="./saved_state/model_gan.ckpt")
            result = sess.run([self.generator_result], feed_dict={self.X: self.x,  self.Z: self.sample_z(self.batch_size,
                                                                                                         self.z_dimension)})
        return result
